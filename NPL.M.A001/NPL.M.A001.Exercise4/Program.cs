﻿internal class Program
{
    static void CheckPrimeNumber(int input)
    {
        if (input == 2)
        {
            Console.WriteLine("Inputed number is prime !\n");
            return;
        }
        else
        {
            for (int i = 2; i < input; i++)
            {
                if (input % i == 0)
                {
                    Console.WriteLine("Inputed number is not prime !\n");
                    return;
                }
            }
        }
        Console.WriteLine("Inputed number is prime !\n");
    }
    private static void Main(string[] args)
    {
        Console.Write("\nQ4:\nInput a number you want to check: ");
        string Input = Console.ReadLine();
        CheckPrimeNumber(Convert.ToInt32(Input));
    }
}