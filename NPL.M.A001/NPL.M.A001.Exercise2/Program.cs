﻿internal class Program
{

    static string ConverToBinary(int dec)
    {
        string bin = "";
        string rebin = "";
        int dec1 = dec;
        while (dec1 > 0)
        {
            rebin = rebin + dec1 % 2;
            dec1 = dec1 / 2;
        }
        for (int i = rebin.Length - 1; i >= 0; i--)
        {
            bin = bin + rebin[i];
        }
        return bin;
    }
    private static void Main(string[] args)
    {
        Console.Write("\nQ2:\nInput a number you want to convert: ");
        string Input = Console.ReadLine();
        Console.WriteLine("Convert to Binary: " + ConverToBinary(Convert.ToInt32(Input)));
    }
}