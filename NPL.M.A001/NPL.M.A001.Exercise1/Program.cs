﻿internal class Program
{
    static void Polynomial(float a, float b, float c)
    {
        if (a == 0)
        {
            if (b == 0)
            {
                if(c!=0){
                    Console.WriteLine("No Solution !\n");
                }else{
                    Console.WriteLine("Infinite Solution !\n");
                }
            }
            else
            {
                Console.Write("x = ", -c / b);
            }
            return;
        }
        float delta = b * b - 4 * a * c;
        float x1;
        float x2;

        if (delta > 0)
        {
            x1 = (float)((-b + Math.Sqrt(delta)) / (2 * a));
            x2 = (float)((-b - Math.Sqrt(delta)) / (2 * a));
            Console.WriteLine("x1 =" + x1 + "\nx2 = " + x2);
        }
        else if (delta == 0)
        {
            x1 = -b / (2 * a);
            Console.WriteLine("x1 = x2 = " + x1);
        }
        else
        {
            Console.WriteLine("No Solution\n");
        }
    }
    private static void Main(string[] args)
    {
        int a, b, c;
        Console.Write("Q1:\nInput a = ");
        string Input = Console.ReadLine();
        a = Convert.ToInt32(Input);
        Console.Write("Input b = ");
        Input = Console.ReadLine();
        b = Convert.ToInt32(Input);
        Console.Write("Input c = ");
        Input = Console.ReadLine();
        c = Convert.ToInt32(Input);
        Polynomial(a, b, c);
    }
}