﻿internal class Program
{
    static void PrintFibonacci(int n)
    {
        int x1 = 0;

        int x2 = 1;
        int holder = x2;
        for (int i = 0; i < n; i++)
        {
            Console.WriteLine(x2);
            x2 += x1;
            x1 = holder;
            holder = x2;
        }

    }
    
    private static void Main(string[] args)
    {
        Console.Write("\nQ3:\nInput a number of element: ");
        string Input = Console.ReadLine();
        Console.WriteLine("Fibonancci: ");
        PrintFibonacci(Convert.ToInt32(Input));
    }
}