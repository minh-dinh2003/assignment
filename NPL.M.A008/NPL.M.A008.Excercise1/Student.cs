﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A008.Excercise1
{
    internal class Student
    {
        public string Name {  get; set; }
        public string Class {  get; set; }
        public string Gender { get; set; }
        public string Relationship { get; set; }
        public DateOnly EntryDate { get; set; }
        public int Age { get; set; }
        public string Address {  get; set; }
        public decimal Mark {  get; set; }
        public string Grade { get; set; }

        public Student()
        {
        }

        public Student(string name, string @class, string gender, DateOnly entryDate, int age, string address, string relationship = "Single", decimal mark = 0, string grade="F")
        {
            Name = name;
            Class = @class;
            Gender = gender;
            Relationship = relationship;
            EntryDate = entryDate;
            Age = age;
            Address = address;
            Mark = mark;
            Grade = grade;
        }
        public void Graduate(decimal Mark =0M)
        {
            if(Mark >= 0 && Mark<=0.9M)
            {
                this.Grade = "F";
            }else if(Mark >=1.0M && Mark<=1.9M) 
            { 
                this.Grade = "D";
            }else if(Mark >= 2M && Mark <= 2.2M){
                this.Grade = "C";
            }
            else if (Mark >= 2.3M && Mark <= 2.6M)
            {
                this.Grade = "C+";
            }
            else if (Mark >= 2.7M && Mark <= 2.9M)
            {
                this.Grade = "B-";
            }
            else if (Mark >= 3.0M && Mark <= 3.2M)
            {
                this.Grade = "B";
            }
            else if (Mark >= 3.3M && Mark <= 3.6M)
            {
                this.Grade = "B+";
            }
            else if (Mark >= 3.7M && Mark <= 3.9M)
            {
                this.Grade = "A-";
            }
            else
            {
                this.Grade = "A";
            }
        }
        public string toString ()
        {
            return string.Format("{0,-10}{1,-11}{2,-10}{3,-15}{4,-10}{5,-6}",Name,Class,Gender,Relationship,Age,Grade);
        }
    }
}
