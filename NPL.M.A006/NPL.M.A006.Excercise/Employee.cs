﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A006.Excercise
{
    internal abstract class Employee
    {
        public string SSN { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BirthDate { get; set; }
        public string Phone {  get; set; }
        public string Email { get; set; }

        public Employee()
        {
        }

        public Employee(string sSN, string firstName, string lastName)
        {
            SSN = sSN;
            FirstName = firstName;
            LastName = lastName;
        }

        public Employee(string sSN, string firstName, string lastName, string birthDate, string phone, string email) : this(sSN, firstName, lastName)
        {
            SSN = sSN;
            FirstName = firstName;
            LastName = lastName;
            BirthDate = birthDate;
            Phone = phone;
            Email = email;
        }

        public void Display()
        {
            Console.WriteLine("{0,-8}{1,-15}{2,-15}{3,-15}{4,-14}{5,-20}",this.SSN,this.FirstName,this.LastName,this.BirthDate,this.Phone,this.Email);
        }
    }
}
