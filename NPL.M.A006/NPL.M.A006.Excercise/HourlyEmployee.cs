﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A006.Excercise
{
    internal class HourlyEmployee:Employee
    {
        public HourlyEmployee(string sSN, string firstName, string lastName, string birthDate, string phone, string email, double Wage, double WorkingHour) : base(sSN, firstName, lastName, birthDate, phone, email)
        {
            this.SSN = sSN;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.BirthDate = birthDate;
            this.Phone = phone;
            this.Email = email;
            this.Wage = Wage;
            this.WorkingHour = WorkingHour;
        }

        double Wage {  get; set; }
        double WorkingHour {  get; set; }
        public void Display()
        {
            Console.WriteLine("{0,-8}{1,-15}{2,-15}{3,-15}{4,-14}{5,-19}{6,-8}{7,-15}", this.SSN, this.FirstName, this.LastName, this.BirthDate, this.Phone, this.Email,this.Wage,this.WorkingHour);
        }
    }
}
