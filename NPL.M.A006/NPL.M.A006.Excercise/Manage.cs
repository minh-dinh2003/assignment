﻿using System.Text.RegularExpressions;

namespace NPL.M.A006.Excercise
{
    internal class Manage
    {
        List<Employee> employees = new List<Employee>();
        List<HourlyEmployee> hours = new List<HourlyEmployee>();
        List<SalariedEmployee> salariedEmployees = new List<SalariedEmployee>();
        public void Menu()
        {
            Console.WriteLine("=======Assignment 06 - EmployeeManagement=======");
            Console.WriteLine("Please select the admin area you require:");
            Console.WriteLine("1. Import Employee.");
            Console.WriteLine("2. Display Employee Information.");
            Console.WriteLine("3. Search Employee.");
            Console.WriteLine("4. Exit");
            Console.Write("Enter Menu Option Number: ");
        }
        public void ShowEmployeeList()
        {
            foreach (Employee emp in employees)
            {
                emp.Display();
            }
        }
        public void Search()
        {
            Regex rgChoice = new Regex("[1|2|3]");
            int choice = 0;
            string inputChoice;
            Console.WriteLine("1. By Employee Type.");
            Console.WriteLine("2. By Employee Name.");
            Console.WriteLine("3. Main Menu.");
            do
            {
                Console.Write("Enter Menu Option Number: ");
                inputChoice = Console.ReadLine();
                if (rgChoice.IsMatch(inputChoice))
                {
                    choice = Convert.ToInt32(inputChoice);
                }
            } while (choice == 0);
            switch (choice)
            {
                case 1:
                    Console.Write("Enter Employee Type: ");
                    string type = Console.ReadLine();
                    if (type.CompareTo("Hourly") == 0)
                    {
                        Console.WriteLine("SSN     FirstName      LastName       BirthDate      Phone         Email              Wage    Working Hour");
                        ShowHourlyEmployee();
                    }
                    if (type.CompareTo("Salaried") == 0)
                    {
                        Console.WriteLine("SSN     FirstName      LastName       BirthDate      Phone         Email              Comm Rate      Gross Sales    Basic Sal");
                        ShowSalariedEmployee();
                    }
                    break;
                case 2:
                    Console.Write("Enter Employee Name: ");
                    string name = Console.ReadLine();
                    Console.WriteLine("SSN     FirstName      LastName       BirthDate      Phone         Email");
                    ShowList(SearchByName(name));
                    break;
                case 3: return;
            }
        }
        public void ShowHourlyEmployee()
        {
            foreach (HourlyEmployee emp in hours)
            {
                emp.Display();
            }
        }
        public void ShowSalariedEmployee()
        {
            foreach (SalariedEmployee emp in salariedEmployees)
            {
                emp.Display();
            }
        }       
        public void ShowList(List<Employee> list)
        {
            foreach (Employee emp in list)
            {
                emp.Display();
            }
        }
        public List<Employee> SearchByName(string name)
        {
            List<Employee> list = new List<Employee>();
            foreach (Employee emp in employees)
            {
                if (emp.FirstName.CompareTo(name) == 0 || emp.LastName.CompareTo(name) == 0)
                {
                    list.Add(emp);
                }
            }
            return list;
        }

        public void ImportEmployee()
        {


            Regex rgName = new Regex(@"[a-zA-Z ]"),
                rgEmail = new Regex("^[A-Za-z]+\\.?[^ ]+@[A-Za-z0-9.-]+[A-Za-z0-9]$"),
                rgDate = new Regex("^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\\d\\d$"),
                rgDouble = new Regex("-?\\d+[\\.?\\d+]*"),
                rgPhone = new Regex("[0-9]{7,}"),
                rgSSN = new Regex("[1-9]");
            string fName, lName, Email, DOB, phone, inputType, SSN;
            Console.WriteLine("========Import Employee========");
            Console.WriteLine("1. Salaried Employee");
            Console.WriteLine("2. Hourly Employee");
            Console.WriteLine("3. Menu");
            Regex rgType = new Regex("1|2|3");

            do
            {
                Console.Write("Enter Menu Option Number: ");
                inputType = Console.ReadLine();
            } while (!rgType.IsMatch(inputType));
            int type = Convert.ToInt32(inputType);
            if (type == 3)
            {
                return;
            }
            bool checkSsn=false;
            do
            {
                checkSsn = false;
                Console.Write("Input SSN: ");
                SSN = Console.ReadLine();
                if (employees.Count == 0)
                {
                    checkSsn = false;
                }
                else
                {
                    foreach (Employee e in employees)
                    {
                        if (e.SSN.CompareTo(SSN) == 0)
                        {
                            checkSsn = true;
                            break;
                        }
                    }
                }
            } while (!rgSSN.IsMatch(SSN)||checkSsn);
            do
            {
                Console.Write("Input FirstName: ");
                fName = Console.ReadLine();
            } while (!rgName.IsMatch(fName));
            do
            {
                Console.Write("Input LastName: ");
                lName = Console.ReadLine();
            } while (!rgName.IsMatch(lName));
            do
            {
                Console.Write("Input Email: ");
                Email = Console.ReadLine();
            } while (!rgEmail.IsMatch(Email));
            do
            {
                Console.Write("Input Birth: ");
                DOB = Console.ReadLine();
            } while (!rgDate.IsMatch(DOB));
            do
            {
                Console.Write("Input Phone: ");
                phone = Console.ReadLine();
            } while (!rgPhone.IsMatch(phone));
            if (type == 1)
            {
                string commissionRate, grossSale, basicSalary;
                do
                {
                    Console.Write("Input CommissionRate: ");
                    commissionRate = Console.ReadLine();
                } while (!rgDouble.IsMatch(commissionRate));
                do
                {
                    Console.Write("Input Gross Sale: ");
                    grossSale = Console.ReadLine();
                } while (!rgDouble.IsMatch(grossSale));
                do
                {
                    Console.Write("Input Basic Salary: ");
                    basicSalary = Console.ReadLine();
                } while (!rgDouble.IsMatch(basicSalary));
                SalariedEmployee employee = new SalariedEmployee(SSN, fName, lName, DOB, phone, Email, Convert.ToDouble(commissionRate), Convert.ToDouble(grossSale), Convert.ToDouble(basicSalary));
                employees.Add(employee);
                salariedEmployees.Add(employee);
            }
            else
            {
                string wage, workingHour;
                do
                {
                    Console.Write("Input Wage: ");
                    wage = Console.ReadLine();
                } while (!rgDouble.IsMatch(wage));
                do
                {
                    Console.Write("Input Working Hour: ");
                    workingHour = Console.ReadLine();
                } while (!rgDouble.IsMatch(workingHour));
                HourlyEmployee employee = new HourlyEmployee(SSN, fName, lName, DOB, phone, Email, Convert.ToDouble(wage), Convert.ToDouble(workingHour));
                employees.Add(employee);
                hours.Add(employee);
            }


        }
    }
}
