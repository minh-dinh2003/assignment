﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A006.Excercise
{
    internal class SalariedEmployee:Employee
    {
        double CommissionRate {  get; set; }
        double GrossSales {  get; set; }
        double BasicSalary { get; set; }

        public SalariedEmployee()
        {
        }

        public SalariedEmployee(string sSN, string firstName, string lastName, string birthDate, string phone, string email,double CommissionRate, double GrossSales, double BasicSalary) : base(sSN, firstName, lastName, birthDate, phone, email)
        {
            this.SSN = sSN;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.BirthDate = birthDate;
            this.Phone = phone;
            this.Email = email;
            this.CommissionRate = CommissionRate;
            this.GrossSales = GrossSales;
            this.BasicSalary = BasicSalary;
        }
        public void Display()
        {
            Console.WriteLine("{0,-8}{1,-15}{2,-15}{3,-15}{4,-14}{5,-19}{6,-15}{7,-15}{8,-15}", this.SSN, this.FirstName, this.LastName, this.BirthDate, this.Phone, this.Email, this.CommissionRate, this.GrossSales, this.BasicSalary);
        }
    }
}
