﻿using NPL.M.A006.Excercise;
using System.Text.RegularExpressions;

internal class Program
{
    private static void Main(string[] args)
    {
        Manage manage = new Manage();
        Regex rgOption = new Regex("[1-4]");
        string checkOption;
        int option=0;
        do
        {
            do
            {
                manage.Menu();
                checkOption = Console.ReadLine();
                if(rgOption.IsMatch(checkOption))
                {
                    option = Convert.ToInt32(checkOption);
                }
            }while(option == 0);
            switch (option)
            {
                case 1: 
                    manage.ImportEmployee();
                    break;
                case 2:
                    Console.WriteLine("SSN     FirstName      LastName       BirthDate      Phone         Email");
                    manage.ShowEmployeeList();
                    break;
                case 3:
                    manage.Search();
                    break;
                case 4:
                    return;
            }
        } while (true);
    }
}