﻿internal class Program
{
    static string NormalizeName(string Name)
    {
        if (Name == "" || Name ==null)
        {
            return "Please provide Name";
        }
        else
        {
            string NameNomalize = Name[0].ToString().ToUpper();
            for (int i = 1; i < Name.Length; i++)
            {
                if (Name[i] != ' ' && Name[i - 1] == ' ')
                {
                    NameNomalize += Name[i].ToString().ToUpper();
                }
                else if (Name[i] == ' ' && Name[i + 1] == ' ')
                {

                }
                else
                {
                    NameNomalize += Name[i].ToString().ToLower();
                }
            }
            return NameNomalize;
        }
    }
    private static void Main(string[] args)
    {
        Console.WriteLine("Input Your Name: ");
        string Name = Console.ReadLine().Trim();
        Console.WriteLine(NormalizeName(Name));
    }
}