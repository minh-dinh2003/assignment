﻿using static System.Formats.Asn1.AsnWriter;
using System.Diagnostics.Metrics;
using System.Numerics;
using System.Reflection;

internal class Program
{
    static string[] SortName(string[] Names)
    {
        string[] SortNames = Names;
        string nameTemp;
        for (int i = 1; i < Names.Length; i++)
        {   
            for(int j = 0;j<Names.Length-i;j++)
            if (Names[j].CompareTo(Names[j+1]) > 0)
            {
                nameTemp = Names[j];
                SortNames[j] = SortNames[j+1];
                SortNames[j + 1] = nameTemp;
            }
        }
        return SortNames;
    }
    static void PrintList(string[] Names)
    {
        for(int i =0;i<Names.Length-1;i++)
        {
            Console.Write(Names[i] +", ");
        }
        Console.WriteLine(Names[Names.Length-1]);
    }
    private static void Main(string[] args)
    {
        Console.Write("Input number of person: ");
        int n = Convert.ToInt32(Console.ReadLine());
        string[] test = new string[n];
        for(int i = 0;i<test.Length;i++)
        {
            Console.Write("Input Name of person "+(i+1)+" : ");
            test[i] = Console.ReadLine();
        }
        Console.WriteLine("List Input: ");
        PrintList(test);
        Console.WriteLine("\nOrdered List: ");
        PrintList(SortName(test));

    }
}