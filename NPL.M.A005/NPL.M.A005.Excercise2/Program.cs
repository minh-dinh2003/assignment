﻿using System.Text.RegularExpressions;

internal class Program
{

    static Boolean IsEmail(string Email)
    {
        if (Email == null || Email =="") 
        {
            return false;
        }
        Regex regex = new Regex("^[A-Za-z0-9]+\\.?[A-Za-z0-9]+@[A-Za-z0-9.-]+[A-Za-z0-9]$");
        Match match = regex.Match(Email);
        if (match.Success ) 
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private static void Main(string[] args)
    {
        Console.WriteLine("Input your Email: ");
        string Email = Console.ReadLine();
        if(IsEmail(Email)) 
        {
            Console.WriteLine(Email+ " is Valid");
        }
        else
        {
            Console.WriteLine(Email + " is not Valid");
        }
    }
}