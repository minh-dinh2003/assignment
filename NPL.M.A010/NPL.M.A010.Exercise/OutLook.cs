﻿using System.Text;

namespace NPL.M.A010.Exercise
{
    public class OutlookMail

    {

        public string From { get; set; }

        public List<string> To { get; set; }

        public List<string> Cc { get; set; }

        public string Subject { get; set; }

        public string AttachmentPath { get; set; }

        public string MailBody { get; set; }

        public bool IsImportant { get; set; }

        public string EncryptedPassword { get; set; }

        public DateTime SentDate { get; set; }

        public MailStatus Status { get; set; }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("From: " + From);
            sb.AppendLine("To: " + string.Join(", ", To));
            sb.AppendLine("Cc: " + string.Join(", ", Cc));
            sb.AppendLine("Subject: " + Subject);
            sb.AppendLine("Attachment: " + AttachmentPath);
            sb.AppendLine("Mail Body: \n" + MailBody);
            sb.AppendLine("Is Important: " + IsImportant);
            sb.AppendLine("Password: " + EncryptedPassword+"\n");

            return sb.ToString();
        }

        public OutlookMail()
        {
        }

        public OutlookMail(string from, List<string> to, List<string> cc, string subject, string attachmentPath, string mailBody, bool isImportant, string encryptedPassword, DateTime sentDate)
        {
            From = from;
            To = to;
            Cc = cc;
            Subject = subject;
            AttachmentPath = attachmentPath;
            MailBody = mailBody;
            IsImportant = isImportant;
            EncryptedPassword = encryptedPassword;
            SentDate = sentDate;
        }
    }
    public enum MailStatus
    {
        Draft,
        Sent
    }

}
