﻿using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace NPL.M.A010.Exercise
{
    internal class Manage
    {
        public static List<OutlookMail> outLooks { get; set; }
        static Manage() { outLooks = new List<OutlookMail>(); }
        public static void GetOutlookMailInput()
        {
            Regex rgPassword = new Regex("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$"),
                rgYN = new Regex("[Y|N|Yes|No]"),
                rgChoice = new Regex("[1|2|3|4]");
            string email, check, password;
            OutlookMail outlookMail = new OutlookMail();
        enterEmail:
            Console.WriteLine("=============Enter email information==============");
            do
            {
                Console.Write("From: ");
                email = Console.ReadLine();
            } while (!ValidateEmail(email));
            outlookMail.From = email;


            string[] toAddresses;
        enterTo:
            Console.Write("To: ");
            toAddresses = Console.ReadLine().Split(',');
            foreach (string toAddress in toAddresses)
            {
                if (!ValidateEmail(toAddress))
                {
                    goto enterTo;
                }
            }


            outlookMail.To = new List<string>(toAddresses);
            string[] ccAddresses;
        enterCC:
            Console.Write("Cc: ");
            ccAddresses = Console.ReadLine().Split(",");
            
            foreach (string ccAddress in ccAddresses)
            {
                if (!ValidateEmail(ccAddress))
                {
                    goto enterCC;
                }
            }
            outlookMail.Cc = new List<string>(ccAddresses);

            Console.Write("Subject: ");
            outlookMail.Subject = Console.ReadLine();

            Console.Write("Attachment: ");
            outlookMail.AttachmentPath = Console.ReadLine();

            outlookMail.MailBody = GetBodyInput();
            do
            {
                Console.Write("Is Important (Yes/No): ");
                check = Console.ReadLine();
            } while (!rgYN.IsMatch(check));
            if (string.Equals(check, "Yes", StringComparison.OrdinalIgnoreCase))
            {
                do
                {
                    Console.Write("Password: ");
                    password = Console.ReadLine();
                } while (!rgPassword.IsMatch(password));
                outlookMail.EncryptedPassword = EncryptPassword(password);
                outlookMail.IsImportant = true;
            }
            else
            {
                outlookMail.IsImportant = false;
            }


            do
            {
                Console.WriteLine("==============Mailing Menu=============");
                Console.WriteLine("1. Send");
                Console.WriteLine("2. Draft");
                Console.WriteLine("3. Re-enter");
                Console.WriteLine("4. Main menu");
                Console.Write("Enter Menu Option Number: ");
                check = Console.ReadLine();
            } while (!rgChoice.IsMatch(check));
            switch (Convert.ToInt32(check))
            {
                case 1:
                    outlookMail.Status = MailStatus.Sent;
                    outlookMail.SentDate = DateTime.Now;
                    break;
                case 2:
                    outlookMail.Status = MailStatus.Draft;
                    break;
                case 3:
                    goto enterEmail;
                case 4:
                    return;
            }
            outLooks.Add(outlookMail);
        }
        public static bool ValidateEmail(string Email)
        {
            Regex rgMail = new Regex("[A-Za-z][A-Za-z0-9._%+-]*@fsoft\\.com\\.vn");
            return rgMail.IsMatch(Email);
        }
        static string GetBodyInput()
        {
            Console.WriteLine("Enter email bodu (Ctrl + Enter when done):");

            StringBuilder sb = new StringBuilder();
            ConsoleKeyInfo keyInfo;

            do
            {
                keyInfo = Console.ReadKey(true);

                if (keyInfo.Key == ConsoleKey.Enter && keyInfo.Modifiers == ConsoleModifiers.Control)
                {
                    break;
                }

                if (keyInfo.Key == ConsoleKey.Enter)
                {
                    sb.AppendLine();
                    Console.WriteLine();
                }
                else if (keyInfo.Key == ConsoleKey.Backspace)
                {
                    if (sb.Length > 0)
                    {
                        sb.Length--;
                        Console.Write("\b \b");
                    }
                }
                else
                {
                    sb.Append(keyInfo.KeyChar);
                    Console.Write(keyInfo.KeyChar);
                }

            } while (true);
            Console.WriteLine("");
            return sb.ToString();
        }
        public static string EncryptPassword(string password)
        {
            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
                byte[] hashedBytes = sha256.ComputeHash(passwordBytes);

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < hashedBytes.Length; i++)
                {
                    builder.Append(hashedBytes[i].ToString("x2"));
                }

                return builder.ToString();
            }
        }
        public static List<OutlookMail> ParseXmlToOutlookMailList(string filePath)
        {
            List<OutlookMail> outlookMails = new List<OutlookMail>();
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(filePath);

                XmlNodeList mailNodes = xmlDoc.SelectNodes("//Mail");
                foreach (XmlNode mailNode in mailNodes)
                {
                    OutlookMail outlookMail = new OutlookMail();

                    outlookMail.From = mailNode.SelectSingleNode("From").InnerText;

                    XmlNodeList toAddressNodes = mailNode.SelectNodes("To/Address");
                    outlookMail.To = new List<string>();
                    foreach (XmlNode addressNode in toAddressNodes)
                    {
                        string toAddress = addressNode.InnerText;
                        outlookMail.To.Add(toAddress);
                    }

                    XmlNodeList ccAddressNodes = mailNode.SelectNodes("Cc/Address");
                    outlookMail.Cc = new List<string>();
                    foreach (XmlNode addressNode in ccAddressNodes)
                    {
                        string ccAddress = addressNode.InnerText;
                        outlookMail.Cc.Add(ccAddress);
                    }

                    outlookMail.Subject = mailNode.SelectSingleNode("Subject").InnerText;
                    outlookMail.AttachmentPath = mailNode.SelectSingleNode("Attachment").InnerText;
                    outlookMail.MailBody = mailNode.SelectSingleNode("Body").InnerText;
                    outlookMail.IsImportant = Convert.ToBoolean(mailNode.SelectSingleNode("IsImportant").InnerText);
                    outlookMail.EncryptedPassword = mailNode.SelectSingleNode("Password").InnerText;
                    DateTime sentDate;
                    if (DateTime.TryParse(mailNode.SelectSingleNode("SentDate")?.InnerText, out sentDate))
                    {
                        outlookMail.SentDate = sentDate;
                    }
                    if (mailNode.SelectSingleNode("Status").InnerText == "Draft")
                        outlookMail.Status = MailStatus.Draft;
                    else outlookMail.Status = MailStatus.Sent;

                    outlookMails.Add(outlookMail);
                }
            }
            catch (Exception ex)
            {
                return outlookMails;
            }

            return outlookMails;
        }
        public static XmlDocument GenerateXmlDocuments(List<OutlookMail> outlookMails)
        {
            XmlDocument xmlDoc = new XmlDocument();

            XmlElement outlookEmailsElement = xmlDoc.CreateElement("OutlookEmails");
            xmlDoc.AppendChild(outlookEmailsElement);

            foreach (OutlookMail outlookMail in outlookMails)
            {
                XmlElement mailElement = xmlDoc.CreateElement("Mail");
                outlookEmailsElement.AppendChild(mailElement);

                XmlElement fromElement = xmlDoc.CreateElement("From");
                fromElement.InnerText = outlookMail.From;
                mailElement.AppendChild(fromElement);

                XmlElement toElement = xmlDoc.CreateElement("To");
                foreach (string toAddress in outlookMail.To)
                {
                    XmlElement addressElement = xmlDoc.CreateElement("Address");
                    addressElement.InnerText = toAddress;
                    toElement.AppendChild(addressElement);
                }
                mailElement.AppendChild(toElement);

                XmlElement ccElement = xmlDoc.CreateElement("Cc");
                foreach (string ccAddress in outlookMail.Cc)
                {
                    XmlElement addressElement = xmlDoc.CreateElement("Address");
                    addressElement.InnerText = ccAddress;
                    ccElement.AppendChild(addressElement);
                }
                mailElement.AppendChild(ccElement);

                XmlElement subjectElement = xmlDoc.CreateElement("Subject");
                subjectElement.InnerText = outlookMail.Subject;
                mailElement.AppendChild(subjectElement);

                XmlElement attachmentElement = xmlDoc.CreateElement("Attachment");
                attachmentElement.InnerText = outlookMail.AttachmentPath;
                mailElement.AppendChild(attachmentElement);

                XmlElement bodyElement = xmlDoc.CreateElement("Body");
                bodyElement.InnerText = outlookMail.MailBody;
                mailElement.AppendChild(bodyElement);

                XmlElement isImportantElement = xmlDoc.CreateElement("IsImportant");
                isImportantElement.InnerText = outlookMail.IsImportant.ToString();
                mailElement.AppendChild(isImportantElement);

                XmlElement passwordElement = xmlDoc.CreateElement("Password");
                passwordElement.InnerText = outlookMail.EncryptedPassword;
                mailElement.AppendChild(passwordElement);

                XmlElement statusElement = xmlDoc.CreateElement("Status");
                statusElement.InnerText = outlookMail.Status.ToString();
                mailElement.AppendChild(statusElement);

                XmlElement sentDateElement = xmlDoc.CreateElement("SentDate");
                sentDateElement.InnerText = outlookMail.SentDate.ToString();
                mailElement.AppendChild(sentDateElement);
            }

            return xmlDoc;
        }
        public static void SaveXmlDocument(XmlDocument xmlDoc, string filePath)
        {
            XmlTextWriter xmlWriter = new XmlTextWriter(filePath, null);
            xmlWriter.Formatting = Formatting.Indented;
            xmlDoc.Save(xmlWriter);
            xmlWriter.Close();
        }

    }
}
