﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class Truck : Car
    {
        public int Weight { get; set; }
        public Truck(decimal speed, double regularPrice, string color,int Weight) : base(speed, regularPrice, color)
        {
            this.Weight = Weight;
        }
        public override double GetSalePrice()
        {
            if(Weight > 2000)
            {
                return RegularPrice - (RegularPrice * 0.1);
            }
            else
            {
                return RegularPrice - (RegularPrice * 0.2);
            }
        }
        public void DisplayPrice()
        {
            Console.WriteLine( Color + " Truck Price: " + GetSalePrice());
        }
    }
}
