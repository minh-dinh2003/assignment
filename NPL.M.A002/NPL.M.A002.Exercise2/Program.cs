﻿internal class Program
{
    static int FindGCD(int a, int b)
    {
        if (a == 0)
        {
            return b;
        }
        return FindGCD(b % a, a);
    }
    private static void Main(string[] args)
    {
        System.Console.Write("Q2:\nInput first number: ");
        int a = Convert.ToInt32(Console.ReadLine());
        System.Console.Write("Input second number: ");
        int b = Convert.ToInt32(Console.ReadLine());
        System.Console.WriteLine("The GCD of 2 numbers is : "+FindGCD(a,b));
    }
}