﻿internal class Program
{
    static void PrintArray(int[] arr)
    {
        Console.Write("[");
        for (int i = 0; i < arr.Length - 1; i++)
        {
            Console.Write(arr[i] + ", ");
        }
        Console.WriteLine(arr[arr.Length - 1] + "]");
    }
    static int[] InputedArray(int n)
    {
        int[] arr = new int[n];
        for (int i = 0; i < n; i++)
        {
            System.Console.Write("Input item " + (i + 1) + ": ");
            arr[i] = Convert.ToInt32(Console.ReadLine());
        }
        return arr;
    }
    static (int, int) FindMaxMin(int[] arr)
    {
        int Max = arr[0];
        int Min = arr[0];
        foreach (int i in arr)
        {
            if (i > Max)
            {
                Max = i;
            }
            if (i < Min)
            {
                Min = i;
            }
        }
        return (Min, Max);
    }
    private static void Main(string[] args)
    {
        System.Console.Write("Q1:\nInput number of element: ");
        string Input = Console.ReadLine();
        int n = Convert.ToInt32(Input);

        int[] arr = new int[n];
        arr = InputedArray(n);
        System.Console.Write("\nThe array is: ");
        PrintArray(arr);
        System.Console.WriteLine("Min item is: " + FindMaxMin(arr).Item1);
        System.Console.WriteLine("Max item is: " + FindMaxMin(arr).Item2);
    }
}