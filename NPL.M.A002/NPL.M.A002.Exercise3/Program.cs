﻿internal class Program
{
    static int[] InputedArray(int n)
    {
        int[] arr = new int[n];
        for (int i = 0; i < n; i++)
        {
            System.Console.Write("Input item "+(i+1)+": ");
            arr[i] = Convert.ToInt32(Console.ReadLine());
        }
        return arr;
    }
    static void PrintArray(int[] arr)
    {
        Console.Write("[");
        for (int i = 0; i < arr.Length - 1; i++)
        {
            Console.Write(arr[i] + ", ");
        }
        Console.WriteLine(arr[arr.Length - 1] + "]");
    }
    static int FindGCD(int a, int b)
    {
        if (a == 0)
        {
            return b;
        }
        return FindGCD(b % a, a);
    }
    static int FindGCDFromArr(int[] arr)
    {
        int gcd = arr[0];
        for (int i = 1; i < arr.Length; i++)
        {
            gcd = FindGCD(arr[i - 1], arr[i]);
            if (gcd == 1)
            {
                return 1;
            }

        }
        return gcd;
    }
    private static void Main(string[] args)
    {
        System.Console.Write("Q3\nInput number of element: ");
        string Input = Console.ReadLine();
        int n = Convert.ToInt32(Input);

        int[] arr = new int[n];
        arr = InputedArray(n);
        System.Console.Write("\nThe array is: ");
        PrintArray(arr);
        System.Console.WriteLine("The GCD of the array is: " + FindGCDFromArr(arr));
    }
}