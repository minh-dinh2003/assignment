﻿using MinhDDHE176148.De10;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.Unicode;
        Console.InputEncoding = System.Text.Encoding.Unicode;
        SERVICE service = new SERVICE();
        bool tiepTuc = true;

        while (tiepTuc)
        {
            Console.WriteLine("----- MENU -----");
            Console.WriteLine("1. Nhập danh sách đối tượng");
            Console.WriteLine("2. Xuất danh sách đối tượng");
            Console.WriteLine("3. Xuất các ngành học có số kỳ học lớn hơn 6");
            Console.WriteLine("4. Xóa ngành học theo ID");
            Console.WriteLine("0. Thoát");

            Console.Write("Vui lòng chọn một chức năng: ");
            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    {
                        do
                        {
                            NganhHoc nganhHoc = nhapNganhHoc();
                            service.ThemNganhHoc(nganhHoc);

                            Console.Write("Bạn có muốn nhập tiếp không? (Y/N): ");
                            string tiep = Console.ReadLine();
                            if (tiep.ToUpper() == "N")
                            {
                                break;
                                tiepTuc = false;
                            }
                        } while (true);
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Danh sách ngành học:");
                        service.XuatDanhSachNganhHoc();
                        break;
                    }
                case 3:
                    {
                        service.XuatNganhHocCoKyHocLonHon6();
                        break;
                    }
                case 4:
                    {
                        Console.Write("Nhập ID ngành học cần xóa: ");
                        int id = int.Parse(Console.ReadLine());
                        service.XoaNganhHocTheoID(id);
                        break;
                    }
                case 0:
                    {
                        tiepTuc = false;
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Không có chức năng này. Vui lòng chọn lại.");
                        break;
                    }
            }

            Console.WriteLine();
        }

        Console.WriteLine("Chương trình đã kết thúc.");
    }

    static NganhHoc nhapNganhHoc()
    {
        Console.WriteLine("----- Nhập thông tin ngành học -----");
        Console.Write("Nhập ID: ");
        int id = int.Parse(Console.ReadLine());
        Console.Write("Nhập tên ngành học: ");
        string ten = Console.ReadLine();
        Console.Write("Nhập số kỳ học: ");
        int soKyHoc = int.Parse(Console.ReadLine());

        NganhHoc nganhHoc = new NganhHoc(id, ten, soKyHoc);
        return nganhHoc;
    }
}