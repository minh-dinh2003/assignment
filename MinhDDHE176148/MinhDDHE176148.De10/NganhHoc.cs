﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhDDHE176148.De10
{
    internal class NganhHoc
    {
        public int ID { get; set; }
        public string Ten { get; set; }
        public int SoKyHoc { get; set; }

        public NganhHoc() { }

        public NganhHoc(int id, string ten, int soKyHoc)
        {
            ID = id;
            Ten = ten;
            SoKyHoc = soKyHoc;
        }

        public void inThongTin()
        {
            Console.WriteLine("ID: " + ID);
            Console.WriteLine("Tên: " + Ten);
            Console.WriteLine("Số kỳ học: " + SoKyHoc);
        }
    }
}
