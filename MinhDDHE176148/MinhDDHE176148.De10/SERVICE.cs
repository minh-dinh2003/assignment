﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhDDHE176148.De10
{
    internal class SERVICE
    {
        private List<NganhHoc> danhSachNganhHoc;

        public SERVICE()
        {
            danhSachNganhHoc = new List<NganhHoc>();
        }

        public void ThemNganhHoc(NganhHoc nganhHoc)
        {
            danhSachNganhHoc.Add(nganhHoc);
        }

        public void XuatDanhSachNganhHoc()
        {
            foreach (NganhHoc nganhHoc in danhSachNganhHoc)
            {
                nganhHoc.inThongTin();
            }
        }

        public void XuatNganhHocCoKyHocLonHon6()
        {
            Console.WriteLine("Danh sách ngành học có số kỳ học lớn hơn 6:");
            foreach (NganhHoc nganhHoc in danhSachNganhHoc)
            {
                if (nganhHoc.SoKyHoc > 6)
                {
                    nganhHoc.inThongTin();
                }
            }
        }

        public void XoaNganhHocTheoID(int id)
        {
            NganhHoc nganhHoc = danhSachNganhHoc.Find(x => x.ID == id);
            if (nganhHoc != null)
            {
                danhSachNganhHoc.Remove(nganhHoc);
                Console.WriteLine("Đã xóa ngành học có ID: " + id);
            }
            else
            {
                Console.WriteLine("Không tìm thấy ngành học có ID: " + id);
            }
        }
    }
}
