﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhDDHE176148.De5
{
    internal class Covid
    {
        public string MaCovid { get; set; }
        public string Ten { get; set; }
        public int NamPhatHien { get; set; }

        public Covid() { }

        public Covid(string maCovid, string ten, int namPhatHien)
        {
            MaCovid = maCovid;
            Ten = ten;
            NamPhatHien = namPhatHien;
        }

        public void inThongTin()
        {
            Console.WriteLine("Mã Covid: " + MaCovid);
            Console.WriteLine("Tên: " + Ten);
            Console.WriteLine("Năm phát hiện: " + NamPhatHien);
        }
    }
}
