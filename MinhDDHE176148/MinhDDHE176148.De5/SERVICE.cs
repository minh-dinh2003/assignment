﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhDDHE176148.De5
{
    internal class SERVICE
    {
        private List<Covid> danhSachCovid;

        public SERVICE()
        {
            danhSachCovid = new List<Covid>();
        }

        public void ThemCovid(Covid covid)
        {
            // Kiểm tra trùng mã Covid
            if (danhSachCovid.Exists(x => x.MaCovid == covid.MaCovid))
            {
                Console.WriteLine("Mã Covid đã tồn tại. Không thể thêm mới.");
                return;
            }

            danhSachCovid.Add(covid);
        }

        public void XuatDanhSachCovid()
        {
            foreach (Covid covid in danhSachCovid)
            {
                covid.inThongTin();
            }
        }

        public void XuatCovidTheoMaBatDauCO()
        {
            foreach (Covid covid in danhSachCovid)
            {
                if (covid.MaCovid.StartsWith("CO"))
                {
                    covid.inThongTin();
                }
            }
        }

        public void SapXepTheoNamPhatHien()
        {
            danhSachCovid.Sort((x, y) => x.NamPhatHien.CompareTo(y.NamPhatHien));
        }
    }
}
