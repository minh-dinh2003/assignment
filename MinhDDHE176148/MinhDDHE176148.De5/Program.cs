﻿using MinhDDHE176148.De5;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.Unicode;
        Console.InputEncoding = System.Text.Encoding.Unicode;
        SERVICE service = new SERVICE();
        bool tiepTuc = true;

        while (tiepTuc)
        {
            Console.WriteLine("----- MENU -----");
            Console.WriteLine("1. Nhập danh sách đối tượng");
            Console.WriteLine("2. Xuất danh sách đối tượng");
            Console.WriteLine("3. Xuất các đối tượng có mã bắt đầu bằng chữ 'CO'");
            Console.WriteLine("4. Sắp xếp năm phát hiện chủng covid mới theo thứ tự tăng dần");
            Console.WriteLine("0. Thoát");

            Console.Write("Vui lòng chọn một chức năng: ");
            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    {
                        do
                        {
                            Covid covid = nhapCovid();
                            service.ThemCovid(covid);

                            Console.Write("Bạn có muốn nhập tiếp không? (Y/N): ");
                            string tiep = Console.ReadLine();
                            if (tiep.ToUpper() == "N")
                            {
                                break;
                                tiepTuc = false;
                            }
                        } while (true);
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Danh sách các Covid:");
                        service.XuatDanhSachCovid();
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Các Covid có mã bắt đầu bằng chữ 'CO':");
                        service.XuatCovidTheoMaBatDauCO();
                        break;
                    }
                case 4:
                    {
                        Console.WriteLine("Danh sách Covid sau khi được sắp xếp theo năm phát hiện:");
                        service.SapXepTheoNamPhatHien();
                        service.XuatDanhSachCovid();
                        break;
                    }
                case 0:
                    {
                        tiepTuc = false;
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Không có chức năng này. Vui lòng chọn lại.");
                        break;
                    }
            }

            Console.WriteLine();
        }

        Console.WriteLine("Chương trình đã kết thúc.");
    }

    static Covid nhapCovid()
    {
        Covid covid = new Covid();

        Console.Write("Nhập mã Covid: ");
        covid.MaCovid = Console.ReadLine();

        Console.Write("Nhập tên: ");
        covid.Ten = Console.ReadLine();

        while (true)
        {
            Console.Write("Nhập năm phát hiện: ");
            if (int.TryParse(Console.ReadLine(), out int namPhatHien))
            {
                covid.NamPhatHien = namPhatHien;
                break;
            }
            else
            {
                Console.WriteLine("Năm phát hiện phải là một số nguyên. Vui lòng nhập lại.");
            }
        }

        return covid;
    }
}
