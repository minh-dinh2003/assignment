﻿using MinhDDHE176148.De8;
internal class Program
{
    private static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.Unicode;
        Console.InputEncoding = System.Text.Encoding.Unicode;
        SERVICE service = new SERVICE();
        bool tiepTuc = true;

        while (tiepTuc)
        {
            Console.WriteLine("----- MENU -----");
            Console.WriteLine("1. Nhập danh sách đối tượng");
            Console.WriteLine("2. Xuất danh sách đối tượng");
            Console.WriteLine("3. Xuất các Teacher ngành UDPM");
            Console.WriteLine("4. Sắp xếp đối tượng theo Ngành");
            Console.WriteLine("0. Thoát");

            Console.Write("Vui lòng chọn một chức năng: ");
            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    {
                        do
                        {
                            Teacher giaoVien = nhapGiaoVien();
                            service.ThemGiaoVien(giaoVien);

                            Console.Write("Bạn có muốn nhập tiếp không? (Y/N): ");
                            string tiep = Console.ReadLine();
                            if (tiep.ToUpper() == "N")
                            {
                                break;
                                tiepTuc = false;
                            }
                        } while (true);
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Danh sách Giáo viên:");
                        service.XuatDanhSachGiaoVien();
                        break;
                    }
                case 3:
                    {
                        service.XuatGiaoVienNganhUDPM();
                        break;
                    }
                case 4:
                    {
                        service.SapXepGiaoVienTheoNganh();
                        Console.WriteLine("Đã sắp xếp danh sách Giáo viên theo Ngành.");
                        break;
                    }
                case 0:
                    {
                        tiepTuc = false;
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Không có chức năng này. Vui lòng chọn lại.");
                        break;
                    }
            }

            Console.WriteLine();
        }

        Console.WriteLine("Chương trình đã kết thúc.");
    }

    static Teacher nhapGiaoVien()
    {
        Console.Write("Nhập ID: ");
        int id = int.Parse(Console.ReadLine());

        Console.Write("Nhập mã giáo viên: ");
        string maGV = Console.ReadLine();

        Console.Write("Nhập ngành: ");
        string nganh = Console.ReadLine();

        return new Teacher(id, maGV, nganh);
    }
}