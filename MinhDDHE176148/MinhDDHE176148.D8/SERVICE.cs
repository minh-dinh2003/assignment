﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhDDHE176148.De8
{
    internal class SERVICE
    {
        private List<Teacher> danhSachGiaoVien;

        public SERVICE()
        {
            danhSachGiaoVien = new List<Teacher>();
        }

        public void ThemGiaoVien(Teacher giaoVien)
        {
            danhSachGiaoVien.Add(giaoVien);
        }

        public void XuatDanhSachGiaoVien()
        {
            foreach (Teacher giaoVien in danhSachGiaoVien)
            {
                giaoVien.inThongTin();
            }
        }

        public void XuatGiaoVienNganhUDPM()
        {
            Console.WriteLine("Danh sách Giáo viên ngành UDPM:");
            foreach (Teacher giaoVien in danhSachGiaoVien)
            {
                if (giaoVien.Nganh == "UDPM")
                {
                    giaoVien.inThongTin();
                }
            }
        }

        public void SapXepGiaoVienTheoNganh()
        {
            danhSachGiaoVien.Sort((gv1, gv2) => gv1.Nganh.CompareTo(gv2.Nganh));
        }
    }
}
