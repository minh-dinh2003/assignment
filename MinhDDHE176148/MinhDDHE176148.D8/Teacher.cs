﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhDDHE176148.De8
{
    internal class Teacher
    {
        private int ID { get; set; }
        public string MaGV { get; set; }
        public string Nganh { get; set; }

        public Teacher() { }

        public Teacher(int id, string maGV, string nganh)
        {
            ID = id;
            MaGV = maGV;
            Nganh = nganh;
        }

        public void inThongTin()
        {
            Console.WriteLine("ID: " + ID);
            Console.WriteLine("Mã giáo viên: " + MaGV);
            Console.WriteLine("Ngành: " + Nganh);
        }
    }
}
