﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhDDHE176148.De4
{
    internal class LapTop
    {
        private static int count = 0;

        public int ID { get; private set; }
        public string MaLapTop { get; set; }
        public double KichThuocMH { get; set; }

        public LapTop()
        {
            ID = ++count;
        }

        public LapTop(string maLapTop, double kichThuocMH) : this()
        {
            MaLapTop = maLapTop;
            KichThuocMH = kichThuocMH;
        }

        public void inThongTin()
        {
            Console.WriteLine("ID: " + ID);
            Console.WriteLine("Mã Laptop: " + MaLapTop);
            Console.WriteLine("Kích thước màn hình: " + KichThuocMH + " inch");
        }
    }

}
