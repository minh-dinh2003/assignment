﻿using MinhDDHE176148.De4;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.Unicode;
        Console.InputEncoding = System.Text.Encoding.Unicode;
        SERVICE service = new SERVICE();
        bool tiepTuc = true;

        // Khởi tạo sẵn 3 đối tượng Laptop
        service.ThemLapTop(new LapTop("LT001", 15.6));
        service.ThemLapTop(new LapTop("LT002", 14.0));
        service.ThemLapTop(new LapTop("LT003", 13.3));

        while (tiepTuc)
        {
            Console.WriteLine("----- MENU -----");
            Console.WriteLine("1. Nhập danh sách đối tượng");
            Console.WriteLine("2. Xuất danh sách đối tượng");
            Console.WriteLine("3. Xóa đối tượng theo Mã Laptop");
            Console.WriteLine("4. Xuất kích thước màn hình theo khoảng");
            Console.WriteLine("0. Thoát");

            Console.Write("Vui lòng chọn một chức năng: ");
            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    {
                        do
                        {
                            Console.Write("Nhập mã Laptop: ");
                            string maLapTop = Console.ReadLine();

                            Console.Write("Nhập kích thước màn hình (inch): ");
                            double kichThuocMH = double.Parse(Console.ReadLine());

                            LapTop lapTop = new LapTop(maLapTop, kichThuocMH);
                            service.ThemLapTop(lapTop);

                            Console.Write("Bạn có muốn nhập tiếp không? (Y/N): ");
                            string tiep = Console.ReadLine();
                            if (tiep.ToUpper() == "N")
                            {
                                break;
                                tiepTuc = false;
                            }
                        } while (true);
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Danh sách các Laptop:");
                        service.XuatDanhSachLapTop();
                        break;
                    }
                case 3:
                    {
                        Console.Write("Nhập mã Laptop cần xóa: ");
                        string maLapTop = Console.ReadLine();
                        service.XoaLapTopTheoMa(maLapTop);
                        break;
                    }
                case 4:
                    {
                        Console.Write("Nhập kích thước màn hình tối thiểu (inch): ");
                        double min = double.Parse(Console.ReadLine());

                        Console.Write("Nhập kích thước màn hình tối đa (inch): ");
                        double max = double.Parse(Console.ReadLine());

                        Console.WriteLine("Các Laptop có kích thước màn hình từ " + min + " đến " + max + " inch:");
                        service.XuatKichThuocManHinhTheoKhoang(min, max);
                        break;
                    }
                case 0:
                    {
                        tiepTuc = false;
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Không có chức năng này. Vui lòng chọn lại.");
                        break;
                    }
            }

            Console.WriteLine();
        }

        Console.WriteLine("Chương trình đã kết thúc.");
    }
}