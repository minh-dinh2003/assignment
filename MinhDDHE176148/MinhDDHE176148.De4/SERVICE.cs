﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhDDHE176148.De4
{
    internal class SERVICE
    {
        private List<LapTop> danhSachLapTop;

        public SERVICE()
        {
            danhSachLapTop = new List<LapTop>();
        }

        public void ThemLapTop(LapTop lapTop)
        {
            danhSachLapTop.Add(lapTop);
            danhSachLapTop.Sort((x, y) => x.KichThuocMH.CompareTo(y.KichThuocMH));
        }

        public void XuatDanhSachLapTop()
        {
            foreach (LapTop lapTop in danhSachLapTop)
            {
                lapTop.inThongTin();
            }
        }

        public void XoaLapTopTheoMa(string maLapTop)
        {
            LapTop lapTopToRemove = null;
            foreach (LapTop lapTop in danhSachLapTop)
            {
                if (lapTop.MaLapTop == maLapTop)
                {
                    lapTopToRemove = lapTop;
                    break;
                }
            }

            if (lapTopToRemove != null)
            {
                danhSachLapTop.Remove(lapTopToRemove);
                Console.WriteLine("Đã xóa Laptop có mã: " + maLapTop);
            }
            else
            {
                Console.WriteLine("Không tìm thấy Laptop có mã: " + maLapTop);
            }
        }

        public void XuatKichThuocManHinhTheoKhoang(double min, double max)
        {
            foreach (LapTop lapTop in danhSachLapTop)
            {
                if (lapTop.KichThuocMH >= min && lapTop.KichThuocMH <= max)
                {
                    Console.WriteLine("Mã Laptop: " + lapTop.MaLapTop);
                    Console.WriteLine("Kích thước màn hình: " + lapTop.KichThuocMH + " inch");
                }
            }
        }
    }

}
