﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhDDHE176148.De3
{
    internal class MayTinh
    {
        public string ID { get; set; }
        public string Ten { get; set; }
        public float TrongLuong { get; set; }

        public MayTinh() { }

        public MayTinh(string id, string ten, float trongLuong)
        {
            ID = id;
            Ten = ten;
            TrongLuong = trongLuong;
        }

        public void inThongTin()
        {
            Console.WriteLine("ID: " + ID);
            Console.WriteLine("Tên: " + Ten);
            Console.WriteLine("Trọng lượng: " + TrongLuong + " kg");
        }
    }
}
