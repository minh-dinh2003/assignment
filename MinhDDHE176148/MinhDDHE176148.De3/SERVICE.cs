﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhDDHE176148.De3
{
    internal class SERVICE
    {
        private List<MayTinh> danhSachMayTinh;

        public SERVICE()
        {
            danhSachMayTinh = new List<MayTinh>();
        }

        public void ThemMayTinh(MayTinh mayTinh)
        {
            danhSachMayTinh.Add(mayTinh);
        }

        public void XuatDanhSachMayTinh()
        {
            foreach (MayTinh mayTinh in danhSachMayTinh)
            {
                mayTinh.inThongTin();
            }
        }

        public void XoaMayTinhTheoID(string id)
        {
            MayTinh mayTinhToRemove = null;
            foreach (MayTinh mayTinh in danhSachMayTinh)
            {
                if (mayTinh.ID == id)
                {
                    mayTinhToRemove = mayTinh;
                    break;
                }
            }

            if (mayTinhToRemove != null)
            {
                danhSachMayTinh.Remove(mayTinhToRemove);
                Console.WriteLine("Đã xóa máy tính có ID: " + id);
            }
            else
            {
                Console.WriteLine("Không tìm thấy máy tính có ID: " + id);
            }
        }

        public void XuatTrongLuongMayTinhTheoTen(string ten)
        {
            foreach (MayTinh mayTinh in danhSachMayTinh)
            {
                if (mayTinh.Ten.Contains(ten))
                {
                    Console.WriteLine("Tên: " + mayTinh.Ten);
                    Console.WriteLine("Trọng lượng: " + mayTinh.TrongLuong + " kg");
                }
            }
        }

        public bool KiemTraTenTrung(string ten)
        {
            foreach (MayTinh mayTinh in danhSachMayTinh)
            {
                if (mayTinh.Ten == ten)
                {
                    return true;
                }
            }
            return false;
        }
    }

}
