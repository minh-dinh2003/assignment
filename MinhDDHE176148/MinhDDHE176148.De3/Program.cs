﻿using MinhDDHE176148.De3;
using System;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.Unicode;
        Console.InputEncoding = System.Text.Encoding.Unicode;
        SERVICE service = new SERVICE();
        bool tiepTuc = true;

        while (tiepTuc)
        {
            Console.WriteLine("----- MENU -----");
            Console.WriteLine("1. Nhập danh sách đối tượng");
            Console.WriteLine("2. Xuất danh sách đối tượng");
            Console.WriteLine("3. Xóa đối tượng theo ID");
            Console.WriteLine("4. Xuất trọng lượng máy tính theo tên gần đúng");
            Console.WriteLine("0. Thoát");

            Console.Write("Vui lòng chọn một chức năng: ");
            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    {
                        do
                        {
                            Console.Write("Nhập tên: ");
                            string ten = Console.ReadLine();

                            if (service.KiemTraTenTrung(ten))
                            {
                                Console.WriteLine("Tên đã tồn tại! Vui lòng nhập lại.");
                                continue;
                            }

                            Console.Write("Nhập trọng lượng (kg): ");
                            float trongLuong = float.Parse(Console.ReadLine());

                            string id = GenerateID();
                            MayTinh mayTinh = new MayTinh(id, ten, trongLuong);
                            service.ThemMayTinh(mayTinh);

                            Console.Write("Bạn có muốn nhập tiếp không? (Y/N): ");
                            string tiep = Console.ReadLine();
                            if (tiep.ToUpper() == "N")
                            {
                                break;
                            }
                        } while (true);
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("----- DANH SÁCH ĐỐI TƯỢNG -----");
                        service.XuatDanhSachMayTinh();
                        Console.WriteLine("--------------------------------");
                        break;
                    }
                case 3:
                    {
                        Console.Write("Nhập ID máy tính cần xóa: ");
                        string id = Console.ReadLine();
                        service.XoaMayTinhTheoID(id);
                        break;
                    }
                case 4:
                    {
                        Console.Write("Nhập tên máy tính cần tìm: ");
                        string ten = Console.ReadLine();
                        service.XuatTrongLuongMayTinhTheoTen(ten);
                        break;
                    }
                case 0:
                    {
                        tiepTuc = false;
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Lựa chọn không hợp lệ! Vui lòng chọn lại.");
                        break;
                    }
            }
        }
    }

    private static string GenerateID()
    {
        Random random = new Random();
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return new string(Enumerable.Repeat(chars, 5)
            .Select(s => s[random.Next(s.Length)]).ToArray());
    }
}
