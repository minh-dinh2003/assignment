﻿class GiaoVien
{
    public int ID { get; set; }
    public string Ten { get; set; }
    public double SoGioDay { get; set; }

    public GiaoVien() { }

    public GiaoVien(int id, string ten, double soGioDay)
    {
        ID = id;
        Ten = ten;
        SoGioDay = soGioDay;
    }

    public virtual void inThongTin()
    {
        Console.WriteLine("ID: " + ID);
        Console.WriteLine("Tên: " + Ten);
        Console.WriteLine("Số giờ dạy: " + SoGioDay);
    }
}