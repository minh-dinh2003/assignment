﻿
class GiaoVienPoly : GiaoVien
{
    public string NganhDay { get; set; }

    public GiaoVienPoly() : base() { }

    public GiaoVienPoly(int id, string ten, double soGioDay, string nganhDay) : base(id, ten, soGioDay)
    {
        NganhDay = nganhDay;
    }

    public override void inThongTin()
    {
        base.inThongTin();
        Console.WriteLine("Ngành dạy: " + NganhDay);
    }
}