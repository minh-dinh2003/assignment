﻿
class SERVICE
{
    private List<GiaoVien> danhSachGV;

    public SERVICE()
    {
        danhSachGV = new List<GiaoVien>();
    }

    public void ThemGiaoVien(GiaoVien gv)
    {
        danhSachGV.Add(gv);
    }

    public void XuatDanhSachGiaoVien()
    {
        foreach (GiaoVien gv in danhSachGV)
        {
            gv.inThongTin();
        }
    }

    public void XuatGiaoVienTheoKhoangGio(double gioMin, double gioMax)
    {
        foreach (GiaoVien gv in danhSachGV)
        {
            if (gv.SoGioDay >= gioMin && gv.SoGioDay <= gioMax)
            {
                gv.inThongTin();
            }
        }
    }

    public void XoaGiaoVienTheoID(int id)
    {
        GiaoVien gvToRemove = null;
        foreach (GiaoVien gv in danhSachGV)
        {
            if (gv.ID == id)
            {
                gvToRemove = gv;
                break;
            }
        }

        if (gvToRemove != null)
        {
            danhSachGV.Remove(gvToRemove);
            Console.WriteLine("Đã xóa giáo viên có ID: " + id);
        }
        else
        {
            Console.WriteLine("Không tìm thấy giáo viên có ID: " + id);
        }
    }
}
