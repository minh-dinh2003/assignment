﻿class Program
{
    static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.Unicode;
        Console.InputEncoding = System.Text.Encoding.Unicode;
        SERVICE service = new SERVICE();
        bool tiepTuc = true;

        while (tiepTuc)
        {
            Console.WriteLine("----- MENU -----");
            Console.WriteLine("1. Nhập danh sách đối tượng");
            Console.WriteLine("2. Xuất danh sách đối tượng");
            Console.WriteLine("3. Xuất danh sách GV có giờ dạy theo khoảng");
            Console.WriteLine("4. Xóa đối tượng theo ID");
            Console.WriteLine("5. Kế thừa");
            Console.WriteLine("0. Thoát");

            Console.Write("Vui lòng chọn một chức năng: ");
            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    {
                        do
                        {
                            Console.Write("Nhập ID: ");
                            int id = int.Parse(Console.ReadLine());
                            Console.Write("Nhập tên: ");
                            string ten = Console.ReadLine();
                            Console.Write("Nhập số giờ dạy: ");
                            double soGioDay = double.Parse(Console.ReadLine());

                            Console.Write("Bạn muốn nhập vào thông tin giáo viên (G) hay giáo viên poly (P)? ");
                            string loaiGiaoVien = Console.ReadLine();

                            if (loaiGiaoVien.ToUpper() == "G")
                            {
                                service.ThemGiaoVien(new GiaoVien(id, ten, soGioDay));
                            }
                            else if (loaiGiaoVien.ToUpper() == "P")
                            {
                                Console.Write("Nhập ngành dạy: ");
                                string nganhDay = Console.ReadLine();
                                service.ThemGiaoVien(new GiaoVienPoly(id, ten, soGioDay, nganhDay));
                            }

                            Console.Write("Bạn có muốn nhập tiếp không? (Y/N): ");
                            string tiep = Console.ReadLine();
                            if (tiep.ToUpper() == "N")
                            {
                                break;
                            }
                        } while (true);
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("----- DANH SÁCH ĐỐI TƯỢNG -----");
                        service.XuatDanhSachGiaoVien();
                        Console.WriteLine("--------------------------------");
                        break;
                    }
                case 3:
                    {
                        Console.Write("Nhập số giờ tối thiểu: ");
                        double gioMin = double.Parse(Console.ReadLine());
                        Console.Write("Nhập số giờ tối đa: ");
                        double gioMax = double.Parse(Console.ReadLine());

                        Console.WriteLine("----- DANH SÁCH ĐỐI TƯỢNG -----");
                        service.XuatGiaoVienTheoKhoangGio(gioMin, gioMax);
                        Console.WriteLine("--------------------------------");
                        break;
                    }
                case 4:
                    {
                        Console.Write("Nhập ID của đối tượng cần xóa: ");
                        int id = int.Parse(Console.ReadLine());
                        service.XoaGiaoVienTheoID(id);
                        break;
                    }
                case 5:
                    {
                        GiaoVienPoly gvPoly = new GiaoVienPoly();
                        gvPoly.inThongTin();
                        break;
                    }
                case 0:
                    {
                        tiepTuc = false;
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Chức năng không hợp lệ!");
                        break;
                    }
            }
        }
    }
}