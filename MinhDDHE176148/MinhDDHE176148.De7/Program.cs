﻿using MinhDDHE176148.De7;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.Unicode;
        Console.InputEncoding = System.Text.Encoding.Unicode;
        SERVICE service = new SERVICE();
        bool tiepTuc = true;

        while (tiepTuc)
        {
            Console.WriteLine("----- MENU -----");
            Console.WriteLine("1. Nhập danh sách đối tượng");
            Console.WriteLine("2. Xuất danh sách đối tượng");
            Console.WriteLine("3. Xuất các đối tượng có bao gồm thêm thông tin năm sinh");
            Console.WriteLine("4. Xóa đối tượng theo ID");
            Console.WriteLine("0. Thoát");

            Console.Write("Vui lòng chọn một chức năng: ");
            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    {
                        do
                        {
                            Student sinhVien = nhapSinhVien();
                            service.ThemSinhVien(sinhVien);

                            Console.Write("Bạn có muốn nhập tiếp không? (Y/N): ");
                            string tiep = Console.ReadLine();
                            if (tiep.ToUpper() == "N")
                            {
                                break;
                                tiepTuc = false;
                            }
                        } while (true);
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Danh sách Sinh viên:");
                        service.XuatDanhSachSinhVien();
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Danh sách Sinh viên với thông tin năm sinh:");
                        service.XuatSinhVienCoThongTinNamSinh();
                        break;
                    }
                case 4:
                    {
                        Console.Write("Nhập ID của sinh viên cần xóa: ");
                        int id = int.Parse(Console.ReadLine());
                        service.XoaSinhVienTheoID(id);
                        break;
                    }
                case 0:
                    {
                        tiepTuc = false;
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Không có chức năng này. Vui lòng chọn lại.");
                        break;
                    }
            }

            Console.WriteLine();
        }

        Console.WriteLine("Chương trình đã kết thúc.");
    }

    static Student nhapSinhVien()
    {
        Console.Write("Nhập ID: ");
        int id = int.Parse(Console.ReadLine());

        Console.Write("Nhập tên: ");
        string ten = Console.ReadLine();

        Console.Write("Nhập tuổi: ");
        int tuoi = int.Parse(Console.ReadLine());

        Console.Write("Nhập ngành: ");
        string nganh = Console.ReadLine();

        return new Student(id, ten, tuoi, nganh);
    }
}
