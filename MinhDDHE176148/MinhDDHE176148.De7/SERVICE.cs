﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhDDHE176148.De7
{
    internal class SERVICE
    {
        private List<Student> danhSachSinhVien;

        public SERVICE()
        {
            danhSachSinhVien = new List<Student>();
        }

        public void ThemSinhVien(Student sinhVien)
        {
            danhSachSinhVien.Add(sinhVien);
        }

        public void XuatDanhSachSinhVien()
        {
            foreach (Student sinhVien in danhSachSinhVien)
            {
                sinhVien.inThongTin();
            }
        }

        public void XuatSinhVienCoThongTinNamSinh()
        {
            foreach (Student sinhVien in danhSachSinhVien)
            {
                sinhVien.inThongTin();
                int namSinh = DateTime.Now.Year - sinhVien.Tuoi;
                Console.WriteLine("Năm sinh: " + namSinh);
            }
        }

        public void XoaSinhVienTheoID(int id)
        {
            Student sinhVien = danhSachSinhVien.Find(x => x.ID == id);
            if (sinhVien != null)
            {
                danhSachSinhVien.Remove(sinhVien);
                Console.WriteLine("Đã xóa sinh viên có ID: " + id);
            }
            else
            {
                Console.WriteLine("Không tìm thấy sinh viên có ID: " + id);
            }
        }
    }
}
