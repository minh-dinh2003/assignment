﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhDDHE176148.De7
{
    internal class Student
    {
        public int ID { get; set; }
        public string Ten { get; set; }
        public int Tuoi { get; set; }
        public string Nganh { get; set; }

        public Student() { }

        public Student(int id, string ten, int tuoi, string nganh)
        {
            ID = id;
            Ten = ten;
            Tuoi = tuoi;
            Nganh = nganh;
        }

        public void inThongTin()
        {
            Console.WriteLine("ID: " + ID);
            Console.WriteLine("Tên: " + Ten);
            Console.WriteLine("Tuổi: " + Tuoi);
            Console.WriteLine("Ngành: " + Nganh);
        }
    }
}
