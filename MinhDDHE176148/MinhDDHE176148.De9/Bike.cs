﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhDDHE176148.De9
{
    internal class Bike
    {
        public int ID { get; set; }
        public string Ten { get; set; }
        public string HSX { get; set; }

        public Bike() { }

        public Bike(int id, string ten, string hsx)
        {
            ID = id;
            Ten = ten;
            HSX = hsx;
        }

        public void inThongTin()
        {
            Console.WriteLine("ID: " + ID);
            Console.WriteLine("Tên: " + Ten);
            Console.WriteLine("Hãng sản xuất: " + HSX);
        }
    }

}
