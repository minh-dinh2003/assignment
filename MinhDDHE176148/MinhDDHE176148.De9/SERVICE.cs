﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhDDHE176148.De9
{
    internal class SERVICE
    {
        private List<Bike> danhSachXeMay;

        public SERVICE()
        {
            danhSachXeMay = new List<Bike>();
        }

        public void ThemXeMay(Bike xeMay)
        {
            danhSachXeMay.Add(xeMay);
        }

        public void XuatDanhSachXeMay()
        {
            foreach (Bike xeMay in danhSachXeMay)
            {
                xeMay.inThongTin();
            }
        }

        public void XuatXeMayHangHonda()
        {
            Console.WriteLine("Danh sách xe máy của hãng HONDA:");
            foreach (Bike xeMay in danhSachXeMay)
            {
                if (xeMay.HSX.ToLower() == "honda")
                {
                    xeMay.inThongTin();
                }
            }
        }

        public void SapXepXeMayTheoIDGiamDan()
        {
            danhSachXeMay.Sort((xm1, xm2) => xm2.ID.CompareTo(xm1.ID));
        }
    }
}
