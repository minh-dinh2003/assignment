﻿using MinhDDHE176148.De9;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.Unicode;
        Console.InputEncoding = System.Text.Encoding.Unicode;
        SERVICE service = new SERVICE();
        bool tiepTuc = true;

        while (tiepTuc)
        {
            Console.WriteLine("----- MENU -----");
            Console.WriteLine("1. Nhập danh sách đối tượng");
            Console.WriteLine("2. Xuất danh sách đối tượng");
            Console.WriteLine("3. Xuất các xe máy của hãng HONDA");
            Console.WriteLine("4. Sắp xếp đối tượng theo ID giảm dần");
            Console.WriteLine("0. Thoát");

            Console.Write("Vui lòng chọn một chức năng: ");
            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    {
                        do
                        {
                            Bike xeMay = nhapXeMay();
                            service.ThemXeMay(xeMay);

                            Console.Write("Bạn có muốn nhập tiếp không? (Y/N): ");
                            string tiep = Console.ReadLine();
                            if (tiep.ToUpper() == "N")
                            {
                                break;
                                tiepTuc = false;
                            }
                        } while (true);
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Danh sách xe máy:");
                        service.XuatDanhSachXeMay();
                        break;
                    }
                case 3:
                    {
                        service.XuatXeMayHangHonda();
                        break;
                    }
                case 4:
                    {
                        service.SapXepXeMayTheoIDGiamDan();
                        Console.WriteLine("Đã sắp xếp danh sách xe máy theo ID giảm dần.");
                        break;
                    }
                case 0:
                    {
                        tiepTuc = false;
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Không có chức năng này. Vui lòng chọn lại.");
                        break;
                    }
            }

            Console.WriteLine();
        }

        Console.WriteLine("Chương trình đã kết thúc.");
    }

    static Bike nhapXeMay()
    {
        Console.Write("Nhập ID: ");
        int id = int.Parse(Console.ReadLine());

        Console.Write("Nhập tên: ");
        string ten = Console.ReadLine();

        Console.Write("Nhập hãng sản xuất: ");
        string hsx = Console.ReadLine();

        return new Bike(id, ten, hsx);
    }
}