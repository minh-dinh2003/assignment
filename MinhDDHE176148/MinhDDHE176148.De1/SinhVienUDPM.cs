﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhDDHE176148.De1
{
    class SinhVienUDPM : SinhVien
    {
        public double DiemJava { get; set; }
        public double DiemCsharp { get; set; }

        public SinhVienUDPM(string maSV, string ten, int namSinh, double diemJava, double diemCsharp)
            : base(maSV, ten, namSinh)
        {
            DiemJava = diemJava;
            DiemCsharp = diemCsharp;
        }

        public override void inThongTin()
        {
            base.inThongTin();
            Console.WriteLine("Điểm Java: " + DiemJava);
            Console.WriteLine("Điểm C#: " + DiemCsharp);
        }
    }
}
