﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhDDHE176148.De1
{
    class SERVICE
    {
        private List<SinhVien> danhSachSV;

        public SERVICE()
        {
            danhSachSV = new List<SinhVien>();
        }

        public void ThemSinhVien(SinhVien sv)
        {
            danhSachSV.Add(sv);
        }

        public void XuatDanhSachSinhVien()
        {
            foreach (SinhVien sv in danhSachSV)
            {
                sv.inThongTin();
            }
        }

        public void XuatSinhVienTuoi50TroiLen()
        {
            foreach (SinhVien sv in danhSachSV)
            {
                if (DateTime.Now.Year - sv.NamSinh >= 50)
                {
                    sv.inThongTin();
                }
            }
        }

        public SinhVien TimSinhVienTheoMa(string maSV)
        {
            foreach (SinhVien sv in danhSachSV)
            {
                if (sv.MaSV == maSV)
                {
                    return sv;
                }
            }
            return null;
        }
    }
}
