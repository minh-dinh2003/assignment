﻿using MinhDDHE176148.De1;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.Unicode;
        Console.InputEncoding = System.Text.Encoding.Unicode;
        SERVICE service = new SERVICE();
        bool tiepTuc = true;

        while (tiepTuc)
        {
            Console.WriteLine("----- MENU -----");
            Console.WriteLine("1. Nhập 1 danh sách đối tượng");
            Console.WriteLine("2. Xuất danh sách đối tượng");
            Console.WriteLine("3. Xuất danh sách các SV tuổi từ 50 trở lên");
            Console.WriteLine("4. Tìm SV theo mã");
            Console.WriteLine("5. Kế thừa");

            Console.WriteLine("0. Thoát");

            Console.Write("Vui lòng chọn một chức năng: ");
            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    {
                        do
                        {
                            Console.Write("Nhập mã SV: ");
                            string maSV = Console.ReadLine();
                            Console.Write("Nhập tên: ");
                            string ten = Console.ReadLine();
                            Console.Write("Nhập năm sinh: ");
                            int namSinh = int.Parse(Console.ReadLine());

                            service.ThemSinhVien(new SinhVien(maSV, ten, namSinh));

                            Console.Write("Bạn có muốn tiếp tục nhập (Y/N)? ");
                            string input = Console.ReadLine();
                            if (input.ToUpper() != "Y")
                            {
                                break;
                            }
                        } while (true);

                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Danh sách sinh viên:");
                        service.XuatDanhSachSinhVien();
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Danh sách sinh viên có tuổi từ 50 trở lên:");
                        service.XuatSinhVienTuoi50TroiLen();
                        break;
                    }
                case 4:
                    {
                        Console.Write("Nhập mã SV cần tìm: ");
                        string maSV = Console.ReadLine();

                        SinhVien sv = service.TimSinhVienTheoMa(maSV);
                        if (sv != null)
                        {
                            Console.WriteLine("Thông tin sinhvien có mã SV '" + maSV + "':");
                            sv.inThongTin();
                        }
                        else
                        {
                            Console.WriteLine("Không tìm thấy sinh viên có mã SV '" + maSV + "'.");
                        }
                        break;
                    }
                case 5:
                    {
                        SinhVienUDPM svUDPM = new SinhVienUDPM("SV001", "Nguyen Van A", 1990, 8.5, 9.0);
                        Console.WriteLine("Thông tin sinh viên UDPM:");
                        svUDPM.inThongTin();
                        break;
                    }
                case 0:
                    {
                        tiepTuc = false;
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Lựa chọn không hợp lệ. Vui lòng chọn lại.");
                        break;
                    }
            }

            Console.WriteLine();
        }
    }
}