﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhDDHE176148.De1
{
    class SinhVien
    {
        public string MaSV { get; set; }
        public string Ten { get; set; }
        public int NamSinh { get; set; }

        public SinhVien() { }

        public SinhVien(string maSV, string ten, int namSinh)
        {
            MaSV = maSV;
            Ten = ten;
            NamSinh = namSinh;
        }

        public virtual void inThongTin()
        {
            Console.WriteLine("Mã SV: " + MaSV);
            Console.WriteLine("Tên: " + Ten);
            Console.WriteLine("Năm sinh: " + NamSinh);
        }
    }
}
