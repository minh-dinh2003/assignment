﻿using MinhDDHE176148.De6;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.Unicode;
        Console.InputEncoding = System.Text.Encoding.Unicode;
        SERVICE service = new SERVICE();
        bool tiepTuc = true;

        while (tiepTuc)
        {
            Console.WriteLine("----- MENU -----");
            Console.WriteLine("1. Nhập danh sách đối tượng");
            Console.WriteLine("2. Xuất danh sách đối tượng");
            Console.WriteLine("3. Xuất các đối tượng có bao gồm thêm thông tin năm sinh");
            Console.WriteLine("4. Xóa đối tượng theo MaHs");
            Console.WriteLine("0. Thoát");

            Console.Write("Vui lòng chọn một chức năng: ");
            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    {
                        do
                        {
                            HocSinh hocSinh = nhapHocSinh();
                            service.ThemHocSinh(hocSinh);

                            Console.Write("Bạn có muốn nhập tiếp không? (Y/N): ");
                            string tiep = Console.ReadLine();
                            if (tiep.ToUpper() == "N")
                            {
                                break;
                                tiepTuc = false;
                            }
                        } while (true);
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Danh sách các Học sinh:");
                        service.XuatDanhSachHocSinh();
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Danh sách Học sinh với thông tin năm sinh:");
                        service.XuatHocSinhCoThongTinNamSinh();
                        break;
                    }
                case 4:
                    {
                        Console.Write("Nhập mã HS của học sinh cần xóa: ");
                        string maHs = Console.ReadLine();
                        service.XoaHocSinhTheoMa(maHs);
                        break;
                    }
                case 0:
                    {
                        tiepTuc = false;
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Không có chức năng này. Vui lòng chọn lại.");
                        break;
                    }
            }

            Console.WriteLine();
        }

        Console.WriteLine("Chương trình đã kết thúc.");
    }

    static HocSinh nhapHocSinh()
    {
        Console.Write("Nhập mã HS: ");
        string maHs = Console.ReadLine();

        Console.Write("Nhập tên: ");
        string ten = Console.ReadLine();

        Console.Write("Nhập tuổi: ");
        int tuoi = int.Parse(Console.ReadLine());

        HocSinh hocSinh = new HocSinh(maHs, ten, tuoi);
        return hocSinh;
    }
}
