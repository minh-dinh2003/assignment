﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhDDHE176148.De6
{
    internal class SERVICE
    {
        private List<HocSinh> danhSachHocSinh;

        public SERVICE()
        {
            danhSachHocSinh = new List<HocSinh>();
        }

        public void ThemHocSinh(HocSinh hocSinh)
        {
            danhSachHocSinh.Add(hocSinh);
        }

        public void XuatDanhSachHocSinh()
        {
            foreach (HocSinh hocSinh in danhSachHocSinh)
            {
                hocSinh.inThongTin();
            }
        }

        public void XuatHocSinhCoThongTinNamSinh()
        {
            foreach (HocSinh hocSinh in danhSachHocSinh)
            {
                hocSinh.inThongTin();
                int namSinh = DateTime.Now.Year - hocSinh.Tuoi;
                Console.WriteLine("Năm sinh: " + namSinh);
            }
        }

        public void XoaHocSinhTheoMa(string maHs)
        {
            HocSinh hocSinh = danhSachHocSinh.Find(x => x.MaHs == maHs);
            if (hocSinh != null)
            {
                danhSachHocSinh.Remove(hocSinh);
                Console.WriteLine("Đã xóa học sinh có mã HS: " + maHs);
            }
            else
            {
                Console.WriteLine("Không tìm thấy học sinh có mã HS: " + maHs);
            }
        }
    }

}
