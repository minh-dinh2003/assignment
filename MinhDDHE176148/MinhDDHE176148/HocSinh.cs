﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhDDHE176148.De6
{
    internal class HocSinh
    {
        public string MaHs { get; set; }
        public string Ten { get; set; }
        public int Tuoi { get; set; }

        public HocSinh() { }

        public HocSinh(string maHs, string ten, int tuoi)
        {
            MaHs = maHs;
            Ten = ten;
            Tuoi = tuoi;
        }

        public void inThongTin()
        {
            Console.WriteLine("Mã HS: " + MaHs);
            Console.WriteLine("Tên: " + Ten);
            Console.WriteLine("Tuổi: " + Tuoi);
        }
    }
}
