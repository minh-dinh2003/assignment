﻿using System.Globalization;

class Program
{
    public static string ReadLineOrCtrlEnter()
    {
        string retString = "";
        int curIndex = 0;
        do
        {
            ConsoleKeyInfo readKeyResult = Console.ReadKey(true);



            // handle Ctrl + Enter
            if (readKeyResult.Key == ConsoleKey.Enter && (readKeyResult.Modifiers & ConsoleModifiers.Control) != 0)
            {
                Console.WriteLine();
                return null;
            }


            // handle Enter
            if (readKeyResult.Key == ConsoleKey.Enter)
            {
                Console.WriteLine();
                return retString;
            }



            // handle backspace
            if (readKeyResult.Key == ConsoleKey.Backspace)
            {
                if (curIndex > 0)
                {
                    retString = retString.Remove(retString.Length - 1);
                    Console.Write(readKeyResult.KeyChar);
                    Console.Write(' ');
                    Console.Write(readKeyResult.KeyChar);
                    curIndex--;
                }
            }
            else
            // handle all other keypresses
            {
                retString += readKeyResult.KeyChar;
                Console.Write(readKeyResult.KeyChar);
                curIndex++;
            }
        }
        while (true);
    }

    static void Main()
    {
        List<string> ina = new List<string>();
        string inp = ReadLineOrCtrlEnter();

        while (!String.IsNullOrWhiteSpace(inp))
        {
            ina.Add(inp);
            inp = ReadLineOrCtrlEnter();
        }
        for (int i = 0; i < ina.Count - 1; i++)
        {
            if (Compare(ina[i], ina[i + 1]) > 0)
            {
                inp = ina[i];
                ina[i] = ina[i + 1];
                ina[i + 1] = inp;
            }
        }
        foreach (string line in ina)
        {
            Console.WriteLine(line);
        }

    }
    static int Compare(string x, string y)
    {
        DateTime xDate = ExtractDate(x);
        DateTime yDate = ExtractDate(y);
        return xDate.CompareTo(yDate);
    }

    static DateTime ExtractDate(string line)
    {
        if (line.Contains("at")&&line.Contains("by"))
        {
            string timestampStr = line.Split(new[] { "at " }, StringSplitOptions.None)[1].Split(new[] { " by" }, StringSplitOptions.None)[0];
            DateTime date = DateTime.ParseExact(timestampStr, "dd/MM/yyyy hh:mm:ss tt", CultureInfo.CurrentCulture);
            return date;
        }
        else
        {
            return DateTime.MinValue;
        }
    }
}
