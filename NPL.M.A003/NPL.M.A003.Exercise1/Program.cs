﻿using System;

internal class Program
{
    static void InvoiceNotice(DateTime date)
    {
        int plus = 7;
        if(date.AddDays(plus).DayOfWeek == DayOfWeek.Saturday) { plus=plus+2; }
        if(date.AddDays(plus).DayOfWeek == DayOfWeek.Sunday) { plus++; }
        Console.WriteLine("Date Enter: "+date.ToString("dd/MM/yyyy"));
        Console.WriteLine("1th: "+date.AddDays(plus).ToString("dd/MM/yyyy"));
        plus = plus + 2;
        if (date.AddDays(plus).DayOfWeek == DayOfWeek.Saturday) { plus = plus + 2; }
        if (date.AddDays(plus).DayOfWeek == DayOfWeek.Sunday) { plus++; }
        Console.WriteLine("2nd: " + date.AddDays(plus++).ToString("dd/MM/yyyy"));
        for (int i = 3; i <= 5; i++)
        {
            if (date.AddDays(plus).DayOfWeek == DayOfWeek.Saturday) { plus = plus + 2; }
            if (date.AddDays(plus).DayOfWeek == DayOfWeek.Sunday) { plus++; }
            Console.WriteLine(i+"nd: " + date.AddDays(plus++).ToString("dd/MM/yyyy"));
        }

    }
    private static void Main(string[] args)
    {
        Console.Write("Input the Date: ");
        DateTime date = DateTime.Parse(Console.ReadLine());
        InvoiceNotice(date);
    }
}