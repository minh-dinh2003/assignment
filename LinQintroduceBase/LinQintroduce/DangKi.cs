﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinQintroduce
{
    internal class DangKi
    {
        public int MaSinhVien {  get; set; }
        public int MaMonHoc { get; set; }
        public float Diem {  get; set; }

        public DangKi()
        {
        }

        public DangKi(int maSinhVien, int maMonHoc, float diem)
        {
            MaSinhVien = maSinhVien;
            MaMonHoc = maMonHoc;
            Diem = diem;
        }
    }
}
