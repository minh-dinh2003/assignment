﻿using LinQintroduce;
using System.Text;
using System.Text.RegularExpressions;

Console.OutputEncoding = Encoding.Unicode;

//-Nhập thông tin sinh viên, môn học
//-Thống kê danh sách sinh viên và số lượng môn học
//-Thêm thông tin là điểm từng môn học của sinh viên và tính điểm trung bình các môn học của sinh viên
//-Thống kê môn học có bao nhiêu sinh viên
//-Thống kê số lượng sinh viên
//-Thống kê số lượng môn học có sinh viên
//-Thống kê số lượng môn học không có sinh viên
static void Menu()
{
    Console.WriteLine("1. Them Sinh Vien");
    Console.WriteLine("2. Them Mon Hoc ");
    Console.WriteLine("3. Them Dang Ki");
    Console.WriteLine("4. In Diem TB");
    Console.WriteLine("5. So Luong Sinh Vien Theo Mon ");
    Console.WriteLine("6. So Mon hoc cua tung Sinh Vien");
    Console.WriteLine("7. So Mon Hoc co Sinh Vien hoc");
    Console.WriteLine("8. So mon hoc ko co sinh vien hoc ");
    Console.WriteLine("9. Ket Thuc");
}
string choice;
Manage manage = new Manage();
Regex rgChoice = new Regex("[0-9]");
do
{
    Menu();
    choice = Console.ReadLine();
    if (!rgChoice.IsMatch(choice))
    {
        Console.WriteLine("Wrong input");
    }
    else
    {
        switch (Convert.ToInt32(choice))
        {

            case 1:
                Console.WriteLine("Them Sinh Vien: ");
                manage.ThemSV();
                break;
            case 2:
                Console.WriteLine("Them Mon Hoc: ");
                manage.ThemMonHoc();
                break;
            case 3:
                Console.WriteLine("Them Dang Ki: ");
                manage.ThemDangKi();
                break;
            case 4:
                Console.WriteLine("In Diem Tb: ");
                manage.DiemTB();
                break;
            case 5:
                Console.WriteLine("In So Luong Sinh Vien Theo Mon: ");
                manage.DemSVKH();
                break;
            case 6:
                Console.WriteLine("In So Mon Hoc Theo Sinh Vien: ");
                manage.DemKHSV();
                break;
            case 7:
                Console.WriteLine("In So Mon Hoc co Sinh Vien: ");
                Console.WriteLine(manage.DemKHCoSV());
                break;
            case 8:
                Console.WriteLine("In So Mon Hoc khong co Sinh Vien: ");
                Console.WriteLine(Manage.monHocs.Count() - manage.DemKHCoSV());
                break;
            case 9:
                return;
        }
    }
} while (true);

