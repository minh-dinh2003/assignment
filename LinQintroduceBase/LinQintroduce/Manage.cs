﻿using System.Text.RegularExpressions;

namespace LinQintroduce
{
    internal class Manage
    {
        public static List<SinhVien> sinhViens = new List<SinhVien>();

        public static List<MonHoc> monHocs = new List<MonHoc>();

        public static List<DangKi> DangKis = new List<DangKi>();

        Regex rgInt = new Regex("[0-9]+");
        public void ThemDangKi()
        {
            string maSV, maMH;
            float diem;

            do
            {
                Console.WriteLine("Nhap Ma Sinh Vien: ");
                maSV = Console.ReadLine();
            } while (!rgInt.IsMatch(maSV));

            do
            {
                Console.WriteLine("Nhap Ma Mon Hoc: ");
                maMH = Console.ReadLine();
            } while (!rgInt.IsMatch(maMH));

            do
            {
                Console.WriteLine("Nhap Diem: ");
                diem = float.Parse(Console.ReadLine());
            } while (diem < 0 || diem > 10);

            SinhVien sinhVien = sinhViens.FirstOrDefault(sv => sv.MaSinhVien == Convert.ToInt32(maSV));
            MonHoc monHoc = monHocs.FirstOrDefault(mh => mh.MaMonHoc == Convert.ToInt32(maMH));
            bool daDangKi = DangKis.Any(dk => dk.MaSinhVien == Convert.ToInt32(maSV) && dk.MaMonHoc == Convert.ToInt32(maMH));

            if (sinhVien != null && monHoc != null && !daDangKi)
            {
                DangKi dangKi = new DangKi(sinhVien.MaSinhVien, monHoc.MaMonHoc, diem);
                DangKis.Add(dangKi);
                Console.WriteLine("dang ki thanh cong!");
            }
            else if (daDangKi)
            {
                Console.WriteLine("sinh vien da dang ki mon hoc nay!");
            }
            else
            {
                Console.WriteLine("Sinh Vien hoac Mon Hoc khong ton tai!");
            }
        }

        public void ThemSV()
        {
            string ma, ten, tuoi, khoa;
            bool check;
            do
            {
                check = true;
                do
                {
                    Console.WriteLine("Nhap Ma: ");
                    ma = Console.ReadLine();
                } while (!rgInt.IsMatch(ma));

                if (sinhViens.Count == 0 || !sinhViens.Any(sv => sv.MaSinhVien == Convert.ToInt32(ma)))
                {
                    check = false;
                }

            } while (check);

            Console.WriteLine("Nhap Ten:");
            ten = Console.ReadLine();

            do
            {
                Console.WriteLine("Nhap Tuoi:");
                tuoi = Console.ReadLine();
            } while (!rgInt.IsMatch(tuoi));

            Console.WriteLine("Nhap khoa: ");
            khoa = Console.ReadLine();

            sinhViens.AddRange(new List<SinhVien> { new SinhVien(Convert.ToInt32(ma), ten, Convert.ToInt32(tuoi), khoa) });
        }
        public void ThemMonHoc()
        {
            string ma, ten;
            bool check;
            do
            {
                check = true;
                do
                {
                    Console.WriteLine("Nhap Ma: ");
                    ma = Console.ReadLine();
                } while (!rgInt.IsMatch(ma));

                if (monHocs.Count == 0 || !monHocs.Any(hl => hl.MaMonHoc == Convert.ToInt32(ma)))
                {
                    check = false;
                }

            } while (check);

            Console.WriteLine("Nhap Ten: ");
            ten = Console.ReadLine();

            monHocs.AddRange(new List<MonHoc> { new MonHoc(Convert.ToInt32(ma), ten) });
        }
        public void DemSVKH()
        {
            var danhSachKhoaHoc = (from dk in DangKis
                                   group dk by dk.MaMonHoc into dem
                                   select new
                                   {
                                       MaMonHoc = dem.Key,
                                       SoSV = dem.Count()
                                   })
                                  .Join(monHocs, skh => skh.MaMonHoc, mh => mh.MaMonHoc,
                                        (skh, mh) => new
                                        {
                                            MaMonHoc = skh.MaMonHoc,
                                            TenMonHoc = mh.TenMonHoc,
                                            SoSV = skh.SoSV
                                        })
                                  .Union(monHocs
                                        .Where(mh => !DangKis.Any(dk => dk.MaMonHoc == mh.MaMonHoc))
                                        .Select(mh => new
                                        {
                                            MaMonHoc = mh.MaMonHoc,
                                            TenMonHoc = mh.TenMonHoc,
                                            SoSV = 0
                                        }));

            foreach (var khoaHoc in danhSachKhoaHoc)
            {
                Console.WriteLine(khoaHoc);
            }
        }
        public void DiemTB()
        {
            var diemTrungBinh = sinhViens
                .GroupJoin(DangKis, sv => sv.MaSinhVien, dk => dk.MaSinhVien,
                    (sv, dks) => new { SinhVien = sv, DangKis = dks })
                .Select(svdks => new
                {
                    HoTen = svdks.SinhVien.HoTen,
                    DiemTB = svdks.DangKis.Any() ? svdks.DangKis.Average(dk => dk.Diem) : 0
                });

            foreach (var sv in diemTrungBinh)
            {
                Console.WriteLine(sv);
            }
        }
        public void DemKHSV()
        {
            var DemKhoaHoc = (from dk in DangKis
                              group dk by dk.MaSinhVien into dem
                              select new
                              {
                                  dem.Key,
                                  soKH = dem.Count()
                              })
                              .Join(sinhViens, skh => skh.Key, sv => sv.MaSinhVien,
                                    (skh, sv) => new
                                    {
                                        sv.HoTen,
                                        soKH = skh.soKH
                                    })
                              .Union(sinhViens
                                     .Where(sv => !DangKis.Any(dk => sv.MaSinhVien == dk.MaSinhVien))
                                     .Select(sv => new
                                     {
                                         sv.HoTen,
                                         soKH = 0
                                     }));
            foreach (var dk in DemKhoaHoc)
            {
                Console.WriteLine(dk);
            }
        }
        public int DemKHCoSV()
        {
            var monHocCoSinhVien = (from dk in DangKis
                                    group dk by dk.MaMonHoc into dem
                                    select new
                                    {
                                        dem.Key
                                    }).Count();
            return monHocCoSinhVien;
        }
    }
}
